<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$this->group(['middleware'=> ['auth'], 'namespace' => 'Admin', 'prefix' => 'admin'], function (){
    Route::get('/', 'AdminController@index')->name('admin.home.index');
    Route::get('/balance', 'BalanceController@index')->name('admin.balance.index');
    Route::get('/deposit', 'BalanceController@deposit')->name('admin.balance.deposito');
    Route::get('/historic', 'BalanceController@historic')->name('admin.balance.historico');
    
    Route::post('/deposit_p', 'BalanceController@depositStore')->name('admin.balance.store');

    Route::get('/saque', 'BalanceController@saque')->name('admin.balance.saque');
    Route::post('/saque', 'BalanceController@saqueStore')->name('admin.balance.saqueStore');

    Route::get('/transferencia', 'BalanceController@transfer')->name('admin.balance.transferencia');
    Route::post('/transferencia-confirm', 'BalanceController@transferStore')->name('admin.balance.transferenciaStore');
    Route::post('/transferencia-confirmada', 'BalanceController@transferStoreConfirm')->name('admin.balance.transConfirmStore');


});


Route::get('perfil', 'Admin\UserController@profile')->name('perfil')->middleware('auth');
Route::post('atualizar-perfil', 'Admin\UserController@profileUpdate')->name('update')->middleware('auth');

Route::get('/', 'SiteController@index')->name('home');

Auth::routes();