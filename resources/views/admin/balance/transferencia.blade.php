@extends('adminlte::page')

@section('title', 'Sistema Saldo RF1 - Saldo')

@section('content_header')

    <ol class="breadcrumb">
        <li><a href="{{route('admin.home.index')}}">Dashboard</a></li>
        <li><a href="{{route('admin.balance.historico')}}">Transferência</a></li>
    </ol>
@stop

@section('content')

    <h1 class="h2">Transferir Saldo (Informe o recebedor)</h1>
    <div class="col-lg-5 col-12 col-xs-12">

        <form action="{{route('admin.balance.transferenciaStore')}}" method="post" class="form-group">
            {{csrf_field()}}
            
            <select class="form-control" style="margin: 10px 0 20px 0; font-family: Roobert TRIAL"  name="sender" id="">
                @foreach ($users as $u)
                    <option value="{{$u->email}}">{{$u->email}}</option>
                @endforeach
            </select>

            @include('admin.includes.alerts')
            <button type="submit" class="btn btn-success form-control">Próxima etapa</button>
        </form>

    </div>

@stop