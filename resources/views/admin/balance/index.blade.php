@extends('adminlte::page')

@section('title', 'Sistema Saldo RF1 - Saldo')

@section('content_header')
    <h1>Saldo</h1>

    <ol class="breadcrumb">
        <li><a href="{{route('admin.home.index')}}">Dashboard</a></li>
        <li><a href="{{route('admin.balance.index')}}">Saldo</a></li>
    </ol>
@stop

@section('content')
    @include('admin.includes.alerts')
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <!-- ./col -->
            <div class="box">
                <div class="box-header" style="margin-left: 15px">
                    <a href="{{route('admin.balance.deposito')}}" class="btn btn-success">
                         Recarregar Valor
                    </a>
                    @if($amount > 0)
                        <a href="{{route('admin.balance.saque')}}" class="btn btn-danger">
                            <i class="fa fa-cart-arrow-down"></i> Sacar
                        </a>
                    @endif
                    @if($amount > 0)
                        <a href="{{route('admin.balance.transferencia')}}" class="btn btn-primary">
                            <i class="fa fa-cart-arrow-down"></i> Transferir
                        </a>
                    @endif
                </div>
                <div class="box-body">
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3><sup style="font-size: 20px">R$ </sup>{{number_format($amount, 2, ',','.')}}</h3>

                                <p>Saldo atual</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-cash"></i>
                            </div>
                            <a href="#" class="small-box-footer ">Histórico<i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        {{--Outros cards--}}
        <div>
            {{--            <!-- ./col -->--}}
            {{--            <div class="col-lg-3 col-xs-6">--}}
            {{--                <!-- small box -->--}}
            {{--                <div class="small-box bg-aqua">--}}
            {{--                    <div class="inner">--}}
            {{--                        <h3>0</h3>--}}

            {{--                        <p>Card 1</p>--}}
            {{--                    </div>--}}
            {{--                    <div class="icon">--}}
            {{--                        <i class="ion ion-bag"></i>--}}
            {{--                    </div>--}}
            {{--                    <a href="#" class="small-box-footer">Informações <i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <!-- ./col -->--}}
            {{--            <div class="col-lg-3 col-xs-6">--}}
            {{--                <!-- small box -->--}}
            {{--                <div class="small-box bg-yellow">--}}
            {{--                    <div class="inner">--}}
            {{--                        <h3>44</h3>--}}

            {{--                        <p>User Registrations</p>--}}
            {{--                    </div>--}}
            {{--                    <div class="icon">--}}
            {{--                        <i class="ion ion-person-add"></i>--}}
            {{--                    </div>--}}
            {{--                    <a href="#" class="small-box-footer">Informações<i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <!-- ./col -->--}}
            {{--            <div class="col-lg-3 col-xs-6">--}}
            {{--                <!-- small box -->--}}
            {{--                <div class="small-box bg-red">--}}
            {{--                    <div class="inner">--}}
            {{--                        <h3>65</h3>--}}

            {{--                        <p>Unique Visitors</p>--}}
            {{--                    </div>--}}
            {{--                    <div class="icon">--}}
            {{--                        <i class="ion ion-pie-graph"></i>--}}
            {{--                    </div>--}}
            {{--                    <a href="#" class="small-box-footer">Informações<i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </div>
        {{--Fim cards--}}

        </div>
    </section>
@stop