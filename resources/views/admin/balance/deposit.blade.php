@extends('adminlte::page')

@section('title', 'Sistema Saldo RF1 - Saldo')

@section('content_header')


    <ol class="breadcrumb">
        <li><a href="">Dashboard</a></li>
        <li><a href="">Saldo</a></li>
        <li><a href="">Depositar</a></li>
    </ol>
@stop

@section('content')

    <h1 class="h2">Fazer recarga</h1>
    <div class="col-lg-5 col-12 col-xs-12">

        <form action="{{route('admin.balance.store')}}" method="post" class="form-group">
            {{csrf_field()}}
            <input name="value" style="margin-bottom: 10px" type="text" placeholder="Valor da Recarga" class="form-control m-2">
            @include('admin.includes.alerts')
            <button type="submit" class="btn btn-success form-control">Enviar</button>
        </form>

    </div>
@stop