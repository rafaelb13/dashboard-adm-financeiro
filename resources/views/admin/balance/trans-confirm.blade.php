@extends('adminlte::page')

@section('title', 'Sistema Saldo RF1')

@section('content_header')

    <ol class="breadcrumb">
        <li><a href="{{route('admin.home.index')}}">Dashboard</a></li>
        <li><a href="{{route('admin.balance.index')}}">Saldo</a></li>
        <li><a href="{{route('admin.balance.transferencia')}}">Transferência</a></li> 
    </ol>
@stop

@section('content')

    <h1 class="h2">Confirmar Transferência de Saldo</h1>
    <div class="col-lg-5 col-12 col-xs-12">
    <p><strong>Recebedor: </strong>{{$sender->name}}</p>
    <div style="margin: 20px 0 20px 0">
    <div>Seu saldo é: R$ {{number_format($balance->amount, 2, ',', '.')}}</div>
    </div>
        <form action="{{route('admin.balance.transConfirmStore')}}" method="post" class="form-group">
            {{csrf_field()}}
            <input type="hidden" name="sender_id" value="{{$sender->id}}">
            <input name="value" style="margin-bottom: 10px" type="text" placeholder="Valor a ser transferido" class="form-control m-2">
                @include('admin.includes.alerts')
            <button type="submit" class="btn btn-success form-control">Transferir</button>
        </form>
    </div>

@stop