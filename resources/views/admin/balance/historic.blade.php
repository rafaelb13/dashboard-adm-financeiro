@extends('adminlte::page')

@section('title', 'Sistema Saldo RF1 - Saldo')

@section('content_header')
    <h1>Seu Histórico de transações</h1>

    <ol class="breadcrumb">
        <li><a href="{{route('admin.home.index')}}">Dashboard</a></li>
        <li><a href="{{route('admin.balance.historico')}}">Historico</a></li>
    </ol>
@stop

@section('content')



    <!--Alterar para receber apenas o ID do usuário LOGADO-->


    <div class="box">
        <div class="box-header">
            <small>#Histórico</small>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Valor</th>
                    <th>Total Antes</th>
                    <th>Total Depois</th>
                    <th>ID Transaction</th>
                    <th>Data</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($data as $d)
                    <tr>
                        <td>{{$d->type($d->type)}}</td>
                        <td>R$ {{number_format($d->amount, 2, ',', '.')}}</td>
                        <td>R$ {{number_format($d->total_before, 2, ',', '.')}}</td>
                        <td>R$ {{number_format($d->total_after, 2, ',', '.')}}</td>
                        <td>{{$d->user_id_transaction ? $d->userSender->name : '-' }}</td>
                        <td>{{$d->date}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <div style="display: flex; justify-content: center;">

                {!! $data->links() !!}

            </div>
        </div>
    </div>


@stop