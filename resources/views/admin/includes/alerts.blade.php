@if($errors->any())
    @foreach($errors->all() as $error)
        <p class="text-danger">{{$error}}</p>
    @endforeach
@endif

@if(session('success'))
   <div class="alert  ">
        <p class="lead text-success">{{session('success')}}</p>
    </div>
@endif


@if(session('error'))
   <div class="alert ">
        <p class="lead text-danger">{{session('error')}}</p>
    </div>
@endif

