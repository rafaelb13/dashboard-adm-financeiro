<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <style>
        html, body {
            background-color: #000;
            background-image: url({{asset('bg-site.jpg')}});
            background-repeat: no-repeat;
            background-size: cover;
            color: #f0f0f0;
            font-family: 'Roobert TRIAL', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: start;

        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            margin-left: 40px;
        }

        .title {
            font-size: 64px;
            text-align: left;
        }

        .links > a {
            color: #ffffff;
            margin-left: 40px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ route('home') }}">Home</a>
                <a href="{{ route('perfil') }}">Meu Perfil</a>
            @else
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Registrar</a>
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Sistema Financeiro
        </div>

        <div class="links">
            <a class="btn btn-outline-danger m-0" href="{{route('admin.home.index')}}">Ir para o Sistema</a>
        </div>
    </div>
</div>
</body>
</html>

