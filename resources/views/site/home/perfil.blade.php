@extends('site.layoutperfil.appperfil')

@section('title', 'Meu Perfil')

@section('content')




    <h1 class="text-center display-3">Edição de Perfil</h1>

    <div class="">
        <form class="form-group" action="{{route('update')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <label  class="form-label m-2" for="name">Altera Nome:</label>
            <input value="{{auth()->user()->name}}" class="form-control m-2" type="text" name="name" id="name" placeholder="Nome">

            <label class="form-label m-2" for="email">Email</label>
            <input value="{{auth()->user()->email}}" class="form-control m-2" type="text" name="email" id="email" placeholder="Email">

            <label class="form-label m-2" for="password">Alterar Senha</label>
            <input class="form-control m-2" type="text" name="password" id="password" placeholder="">

            @if(auth()->user()->image != null)
                <img src="{{url('storage/users/'.auth()->user()->image)}}" alt="" style="width: 50px; height: 50px">
            @endif    
            <label class="form-label m-2" for="image">Insira a imagem de perfil</label>
            <input class="form-control-file m-2" type="file" name="image" id="image">

            <input class="btn btn-outline-info m-2" type="submit" value="Atualizar">
            @include('admin.includes.alerts')
        </form>
    </div>

@endsection