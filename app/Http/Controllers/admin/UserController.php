<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FormRequestUpdate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function profile(){

        return view('site.home.perfil');
    }

    public function profileUpdate(FormRequestUpdate $request){

        $user = auth()->user();

        $data = $request->all();

            if ($data['password'] != null){
                $data['password'] = bcrypt($data['password']);
            }else
                unset($data['password']);

        $data['image'] = $user->image;

        if ($request->hasFile('image') && $request->file('image')->isValid() ){

            if ($user->image){
                $name = $user->image;
            }else
                $name = $user->id.kebab_case($user->name);

            $extension = $request->image->extension();
            $nameFile = "{$name}.{$extension}";

            $data['image'] = $nameFile;

            $upload = $request->image->storeAs('users', $nameFile);



            if (!$upload){
                return redirect()
                    ->back()
                    ->with('error', 'Falha ao fazer upload da imagem!');
            }
        }


        $update = auth()->user()->update($data);

        if ($update){
            return redirect()
                ->route('perfil')
                ->with('success', 'Sucesso ao atualizar!');
        }

        return redirect()
            ->route('perfil')
            ->with('error', 'Falha ao atualizar!');
    }
}
