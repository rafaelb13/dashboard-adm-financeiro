<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MoneyValidationFormRequest;
use App\Models\Balance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Historic;
use App\User;


class BalanceController extends Controller
{


    function index(){

        $balance = auth()->user()->balance;
        $amount = $balance ? $balance->amount : 0;

        return view('admin.balance.index', compact('amount'));
    }

    function deposit(){
        return view('admin.balance.deposit');
    }

    function getdata(){
        return Datatables::of(User::query())->make(true);
    }

    function historic(){
        
        $data = auth()->user()->historics()->with(['userSender'])->paginate(5);

        return view('admin.balance.historic', compact('data'));
    }

    function saque(){
        return view('admin.balance.saque');
    }

    function transfer(){
        $users = User::all();
        return view('admin.balance.transferencia', compact('users'));
    }

    function transferConfirm(){
        return view('admin.balance.trans-confirm');
    }

    function depositStore(MoneyValidationFormRequest $request){
        $balance = auth()->user()->balance()->firstOrCreate([]);
        $response = $balance->deposit($request->value);

        if ($response['success']){
            return redirect()
                        ->route('admin.balance.index')
                        ->with('success', $response['message']);
        }else{
            return redirect()
                        ->back()
                        ->with('error', $response['message']);
        }

    }

    function saqueStore(MoneyValidationFormRequest $request){

        $balance = auth()->user()->balance()->firstOrCreate([]);
        $response = $balance->sacar($request->value);

        if ($response['success']){
            return redirect()
                ->route('admin.balance.index')
                ->with('success', $response['message']);
        }else{
            return redirect()
                ->back()
                ->with('error', $response['message']);
        }

    }

    function transferStore(Request $request, User $user){
        if (!$sender = $user->getSender($request->sender)) {
            return redirect()
                ->back()
                ->with('error', 'Usuário informado não foi encontrado');
        }

        if ($sender->id == auth()->user()->id) {
            return redirect()
                ->back()
                ->with('error', 'Não é possivel transferir para você mesmo');
        }

        $balance = auth()->user()->balance;

        return view('admin.balance.trans-confirm', compact('sender', 'balance'));
    }

    //Ultima pagina de transferência
    function transferStoreConfirm(MoneyValidationFormRequest $request, User $user){
        
        if (!$sender = $user->find($request->sender_id)) {
            return redirect()
                ->route('admin.balance.transferencia')
                ->with('success', 'Recebedor não encontrado!');
        }

        $balance = auth()->user()->balance()->firstOrCreate([]);
        $response = $balance->transfer($request->value, $sender);

        if ($response['success']){
            return redirect()
                ->route('admin.balance.index')
                ->with('success', $response['message']);
        }else{
            return redirect()
                ->back()
                ->with('error', $response['message']);
        }
    }


    


}
