<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    function index(){

        $usuario = auth()->user()->name;

        return view('admin.home.index', compact('usuario'));
    }
}
