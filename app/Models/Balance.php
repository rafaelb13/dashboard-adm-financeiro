<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Balance extends Model
{
    public $timestamps = false;



    public function deposit($value){

        DB::beginTransaction();

        $totalBefore    = $this->amount ? $this->amount : 0;
        $this->amount  +=  $value;
        $deposit        = $this->save();

        $historic = auth()->user()->historics()->create([
            'type'                  => 'I',
            'amount'                => $value,
            'total_before'          => $totalBefore,
            'total_after'           => $this->amount,
            'date'                  => date('Ymd'),
        ]);

        if ($deposit && $historic){

            DB::commit();

            return [
                'success'           => true,
                'message'           => 'Sucesso ao recarregar'
            ];
        }else{
            DB::rollback();
            return [
                'success'           => false,
                'message'           => 'Falha ao recarregar'
            ];
        }

    }

    public function sacar($value){

        if ($this->amount < $value){
            return [
                'success'           => false,
                'message'           => 'Saldo Insuficiente!!'
            ];
        }

        DB::beginTransaction();

        $totalBefore     = $this->amount ? $this->amount : 0;
        $this->amount   -=  $value;
        $saque           = $this->save();

        $historic = auth()->user()->historics()->create([
            'type'                  => 'O',
            'amount'                => $value,
            'total_before'          => $totalBefore,
            'total_after'           => $this->amount,
            'date'                  => date('Ymd'),
        ]);

        if ($saque && $historic){

            DB::commit();

            return [
                'success'           => true,
                'message'           => 'Sucesso ao sacar valor!!'
            ];
        }else{
            DB::rollback();
            return [
                'success'           => false,
                'message'           => 'Falha ao sacar valor!!'
            ];
        }
    }


    public function transfer($value, User $sender){

        if ($this->amount < $value){
            return [
                'success'           => false,
                'message'           => 'Saldo Insuficiente!!'
            ];
        }

        DB::beginTransaction();

        //Atualizar o proprio saldo
        $totalBefore = $this->amount ? $this->amount : 0;
        $this->amount -= $value;
        $transfer = $this->save();

        $historic = auth()->user()->historics()->create([
            'type'                  => 'T',
            'amount'                => $value,
            'total_before'          => $totalBefore,
            'total_after'           => $this->amount,
            'date'                  => date('Ymd'),
            'user_id_transaction'   => $sender->id,
        ]);

        //Atualizar o saldo do recebedor
        $senderBalance = $sender->balance()->firstOrCreate([]);
        $sendertotalBefore = $senderBalance->amount ? $senderBalance->amount : 0;
        $senderBalance->amount +=  $value;
        $transferSender = $senderBalance->save();

        $historicSender = $sender->historics()->create([
            'type'                  => 'I',
            'amount'                => $value,
            'total_before'          => $sendertotalBefore,
            'total_after'           => $senderBalance->amount,
            'date'                  => date('Ymd'),
            'user_id_transaction'   => auth()->user()->id,
        ]);

        if ($transfer && $historic && $transferSender && $historicSender){

            DB::commit();

            return [
                'success'           => true,
                'message'           => 'Sucesso ao transferir valor!!'
            ];

        }else{
            
            DB::rollback();
            return [
                'success'           => false,
                'message'           => 'Falha ao transferir valor!!'
            ];
        }

    }
}
