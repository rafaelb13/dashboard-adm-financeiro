<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Historic extends Model
{
    protected $fillable = [
        'type', 'amount', 'total_before', 'total_after', 'user_id_transaction', 'date'
    ];

    public function type($type = null){
        $types = [
            'I' => 'Deposito',
            'O' => 'Saque',
            'T' => 'Transferência',
        ];

        if(!$type){
            return $types;
        }

        return $types[$type];
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function userSender(){
        return $this->belongsTo(User::class, 'user_id_transaction');
    }

    public function getDateAttribute($value){

       return Carbon::parse($value)->format('d/m/Y');

    }
}
