<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
           'name'       => 'Rafael Borges',
           'email'      => 'rafaelsfb@gmail.com',
           'password'   => bcrypt('123456'),
        ]);
        User::create([
            'name'       => 'Bianca Sanches',
            'email'      => 'bianca@gmail.com',
            'password'   => bcrypt('123456'),
        ]);
    }
}
